const getUserByIdQuery = (userId) => {
    try {
        const query = `Select * from usertable where userId = ${userId}`;
        return query;
    } catch(e){
        console.log(`error ${JSON.stringify(e)}`);
    }
};

const createUserQuery = (userInfo) => {
    try {
        const query = `INSERT INTO usertable (userName, userEmail, status)
        VALUES ('${userInfo.name}', '${userInfo.email}', ${userInfo.status})`;
        return query;
    } catch(e){
        console.log(`error ${JSON.stringify(e)}`);
    }
};

const getUserByEmailQuery = (userEmail) => {
    try {
        const query = `Select * from usertable where userEmail = '${userEmail}'`;
        return query;
    } catch(e){
        console.log(`error ${JSON.stringify(e)}`);
    }
};

module.exports = {
    getUserByIdQuery,
    createUserQuery,
    getUserByEmailQuery
}