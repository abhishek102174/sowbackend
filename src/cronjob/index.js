const router = require('express').Router();
const scopeDatabase = require('../models/index')
const scopeVersion = scopeDatabase.scopeVersion
const { isAuthorized } = require('../../utils/auth')
const { sequelize } = require("../models/index");
const moment = require('moment');
const sendEmail = require('../config/mail')

router.get('/', isAuthorized, async function(req, res) {
    try {
        const allScopes = await scopeVersion.findAll();
        allScopes.map(async(scope) => {
                const cuurentDate = moment().format('YYYY-MM-DD');
                const endDate = moment(scope.end_date, 'YYYY-MM-DD');
                const differenceDays = endDate.diff(cuurentDate, 'days')
                console.log(differenceDays);
                if (differenceDays <= 30) {
                    const mailResponse = sendEmail();
                    console.log(mailResponse);
                    console.log('mail sent')
                } else {
                    console.log('More than 1 month');
                }
                const hours = await sequelize.query(`SELECT SUM(time_worked) AS hours_worked FROM time_sheet_details WHERE project_id = 7 AND timesheet_date BETWEEN '2020-08-20' AND '2020-08-24'`)
                console.log(hours);


            })
            // console.log(allScopes);
        res.status(200).send({
            type: "Success",
            message: "Email Cronjob is running",
            data: allScopes
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

module.exports = router;