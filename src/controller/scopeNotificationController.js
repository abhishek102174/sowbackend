const scopeDatabase = require("../models/index");
const scopeNotifictaionEmail = scopeDatabase.scopeNotifictaionEmail
const validator = require('validator');
const moment = require('moment');
class scopeNotificationController {


    static sendEmailtoScopeId(emailLogBody) {
        return new Promise(async(resolve, reject) => {
            try {
                // console.log(JSON.parse(JSON.stringify(emailLogBody.scope_notified_emails)));

                const emails = emailLogBody.scope_notified_emails

                const emailLogtoInsert = {
                    scope_version_id: emailLogBody.scope_version_id,
                    scope_notified_emails: emails,
                    email_sent_date: moment().format('YYYY-MM-DD')
                }
                const emailInsertResult = await scopeNotifictaionEmail.create(emailLogtoInsert);


                resolve(emailInsertResult)






            } catch (error) {
                console.log(error);
                reject(error)
            }
        })
    }

}

module.exports = scopeNotificationController