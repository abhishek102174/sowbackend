const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes } = require('sequelize');
const { sequelize } = require("../models/index");
const { Op } = require("sequelize");
const axios = require('axios');

class loginUserController {

    static loginUser(userBody) {
        return new Promise(async(resolve, reject) => {
            try {
                const username = userBody.username;
                const password = userBody.password;

                const validUser = await axios.get(`https://uat.theskygge.com/BMC/CheckUser?user=${username}&pass=${password}&scope_of_work=true`)
                    //console.log(validUser);
                if (validUser.data.result == 'invalid') {
                    reject(new Error('User credentials are wrong'))
                } else {
                    const query = `SELECT * FROM user_details WHERE user_name = '${username}'`;
                    const userLoginResult = await sequelize.query(query, { type: QueryTypes.SELECT });
                    resolve(userLoginResult);
                }



            } catch (error) {
                reject(error)
            }
        })
    }

    static getProjectsbyUserID(userID) {
        return new Promise(async(resolve, reject) => {
            try {


                const projectResult = await projects.findAll({
                    where: {
                        project_lead: {
                            [Op.in]: [parseInt(userID)]
                        }
                    }
                });
                if (projectResult.length <= 0) {
                    reject(new Error('No projects'))
                } else {
                    resolve(projectResult)
                }




            } catch (error) {
                reject(error)
            }
        })
    }

}

module.exports = loginUserController