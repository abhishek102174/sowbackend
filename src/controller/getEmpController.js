const scopeDatabase = require("../models/index");
const getEmp = scopeDatabase.getEmp.EmpDetails()
const { sequelize } = require("../models/index");
class getEmpController {

    static getAllEmployees() {
        return new Promise(async(resolve, reject) => {
            try {
                const empResult = await getEmp.findAll();
                if (empResult.length <= 0) {
                    reject(new Error('No employees'))
                } else {
                    resolve(empResult)
                }


            } catch (error) {
                reject(error)
            }
        })
    }

    static getAllEmployeesbyID(teamLeadID) {
        return new Promise(async(resolve, reject) => {
            try {

                const empResult = await sequelize.query(`SELECT tl_user_assignment_mapping.tl_id, tl_user_assignment_mapping.user_id, user_details.user_first_name, user_details.user_last_name FROM tl_user_assignment_mapping LEFT JOIN user_details ON tl_user_assignment_mapping.user_id = user_details.user_id WHERE tl_user_assignment_mapping.tl_id = ${parseInt(teamLeadID)}`)
                    //console.log(empResult[0]);
                if (empResult.length <= 0) {
                    reject(new Error('No employees found'))
                } else {
                    resolve(empResult[0])
                }


            } catch (error) {
                reject(error)
            }
        })
    }

}

module.exports = getEmpController