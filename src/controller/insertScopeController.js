const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes, Op } = require('sequelize');
const { sequelize, scope } = require("../models/index");
const scopes = scopeDatabase.scope
const scopeVersion = scopeDatabase.scopeVersion
const scopeEmp = scopeDatabase.scopeEmp
const scopeInv = scopeDatabase.scopeInv
const moment = require('moment');
class insertScopeController {

    static insertScopebyProjectID(scopeBody) {
        return new Promise(async(resolve, reject) => {
            try {

                const existingScopeResult = await scopes.findAll({
                    where: {
                        scope_version: scopeBody.scope_version
                    }
                });
                // console.log(existingScopeResult);
                if (existingScopeResult.length > 0) {
                    reject(new Error('This Scope Version already exists'))
                } else {
                    const scopeUnderProject = {
                        project_id: scopeBody.project_id,
                        scope_version: scopeBody.scope_version,
                        scope_name: scopeBody.scope_name,
                        start_date: scopeBody.start_date,
                        end_date: scopeBody.end_date,
                        total_hours: scopeBody.total_hours,
                        description: scopeBody.description,
                        created_by: scopeBody.created_by,
                        created_at: moment().format('YYYY-MM-DD'),
                        modified_by: scopeBody.modified_by,
                        modified_at: moment().format('YYYY-MM-DD')
                    }
                    const insertScopeResult = await scopes.create(scopeUnderProject);
                    resolve(insertScopeResult)
                    var scopeEmparray = JSON.parse("[" + scopeBody.emp_id + "]");
                    // //console.log(scopeEmparray);
                    scopeEmparray.map(async(empID) => {
                        try {
                            const scopeEmpBody = {
                                scope_version_id: insertScopeResult.scope_version_id,
                                scope_id: insertScopeResult.id,
                                emp_id: empID,
                                status: 1

                            }

                            const insertScopeEmpResult = await scopeEmp.create(scopeEmpBody);

                            //console.log(insertScopeEmpResult);
                        } catch (error) {
                            console.log(error);
                            reject(error)
                        }

                    })
                }


                // const scopeVersionBody = {
                //     scope_id: insertScopeResult.id,
                //     scope_name: scopeBody.scope_name,
                //     start_date: scopeBody.start_date,
                //     end_date: scopeBody.end_date,
                //     total_number_of_hours: scopeBody.total_number_of_hours,
                //     description: scopeBody.description,
                //     created_by: scopeBody.created_by,
                //     created_at: moment().format('YYYY-MM-DD')

                // }

                //const insertScopeVersionResult = await scopeVersion.create(scopeVersionBody);


                // const scopeInvBody = {
                //     scope_version_id: insertScopeVersionResult.scope_version_id,
                //     invoice_id: scopeBody.invoice_id,
                //     raised_hours: scopeBody.raised_hours
                // }
                // const insertScopeInvResult = await scopeInv.create(scopeInvBody);
                // console.log(insertScopeInvResult);






            } catch (error) {
                console.log(error);
                reject(error)
            }
        })
    }



    static insertSubScope(subScopeBody) {
        return new Promise(async(resolve, reject) => {
            try {


                const scopeVersionBody = {
                    scope_id: subScopeBody.scope_id,
                    sub_scope_name: subScopeBody.scope_name,
                    end_date: subScopeBody.end_date,
                    total_number_of_hours: subScopeBody.total_number_of_hours,
                    description: subScopeBody.description,
                    created_by: subScopeBody.created_by,
                    created_at: moment().format('YYYY-MM-DD')

                }

                const insertScopeVersionResult = await scopeVersion.create(scopeVersionBody);
                var scopeEmparray = JSON.parse("[" + subScopeBody.emp_id + "]");
                //console.log(scopeEmparray);
                scopeEmparray.map(async(empID) => {
                    try {
                        const scopeEmpBody = {
                            scope_version_id: insertScopeVersionResult.scope_version_id,
                            emp_id: empID,
                            status: 1

                        }

                        const insertScopeEmpResult = await scopeEmp.create(scopeEmpBody);

                        //console.log(insertScopeEmpResult);
                    } catch (error) {
                        console.log(error);
                        reject(error)
                    }

                })

                // const scopeInvBody = {
                //     scope_version_id: insertScopeVersionResult.scope_version_id,
                //     invoice_id: subScopeBody.invoice_id,
                //     raised_hours: subScopeBody.raised_hours
                // }
                // const insertScopeInvResult = await scopeInv.create(scopeInvBody);
                // console.log(insertScopeInvResult);
                resolve(insertScopeVersionResult)





            } catch (error) {
                console.log(error);
                reject(error)
            }
        })
    }
}

module.exports = insertScopeController