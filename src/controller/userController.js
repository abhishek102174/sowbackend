const {
    getUserById,
    createUserService,
    updateUser
} = require('../middlewares/users');

const {
    ResourceNotFoundError,
    ConflictError,
    InternalError,
    BadRequest,
    UnauthorizedError
} = require('../helpers/errorHandler');

async function fetchUserInfo(req, res){
    try {
        console.log(req.query);
        const { userId } = req.query;
        console.log(userId);
        const result = await getUserById(userId);
        return result;
    } catch(e){
        console.log(e);
    }
}

async function updateUserInfo(req, res){
    try {
        console.log(req.query);
        const body = req.body
        const { userId } = req.query;
        console.log(userId);
        const result = await updateUser(userId, body);
        return result;
    } catch(e){
        console.log(e);
    }
}

async function createUser(req, res){
    try {
        const body = req.body
        console.log(`body ${req}`);
        const result = await createUserService(body);
        res.status(200);
        res.type('json');
        res.write(JSON.stringify(result));
        return res;
    } catch(e){
        console.log(e);
    } finally {
        res.end();
    }
}


module.exports = {
    fetchUserInfo,
    updateUserInfo,
    createUser
};