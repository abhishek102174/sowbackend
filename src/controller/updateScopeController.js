const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes } = require('sequelize');
const { sequelize, scope } = require("../models/index");
const scopes = scopeDatabase.scope
const scopeVersion = scopeDatabase.scopeVersion
const scopeEmp = scopeDatabase.scopeEmp
const scopeInv = scopeDatabase.scopeInv
const moment = require('moment');

class updateScopeController {
    static updateScopebyScopeID(updatescopeBody) {
        return new Promise(async(resolve, reject) => {
            try {
                const scopeVersionID = updatescopeBody.scope_version_id
                const updateScopeUnderProject = {
                    sub_scope_name: updatescopeBody.scope_name,
                    end_date: updatescopeBody.end_date,
                    total_number_of_hours: updatescopeBody.total_number_of_hours,
                    description: updatescopeBody.description,
                    created_by: updatescopeBody.modified_by,
                    created_at: moment().format('YYYY-MM-DD')
                }
                const updateScopeResult = await scopeVersion.update(updateScopeUnderProject, {
                    where: { scope_version_id: updatescopeBody.scope_version_id }
                })
                const deleteEmpbeforeUpdate = await sequelize.query(`DELETE FROM scope_employee_map WHERE scope_version_id = ${scopeVersionID}`);
                var updateScopeEmparray = JSON.parse("[" + updatescopeBody.emp_id + "]");
                updateScopeEmparray.map(async(empID) => {
                        try {
                            const scopeEmpBody = {
                                scope_version_id: scopeVersionID,
                                emp_id: empID,
                                status: 1

                            }

                            const insertScopeEmpResult = await scopeEmp.create(scopeEmpBody);

                            //console.log(insertScopeEmpResult);
                        } catch (error) {
                            console.log(error);
                            reject(error)
                        }

                    })
                    // const updateScopeInvBody = {
                    //     scope_version_id: scopeVersionID,
                    //     invoice_id: updatescopeBody.invoice_id,
                    //     raised_hours: updatescopeBody.raised_hours
                    // }
                    // const updateScopeInvResult = await scopeInv.create(updateScopeInvBody);
                resolve(updateScopeResult);

            } catch (error) {
                console.log(error);
            }
        })
    }
}

module.exports = updateScopeController