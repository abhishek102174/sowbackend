const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes } = require('sequelize');
const { sequelize } = require("../models/index");
const scopes = scopeDatabase.scope
class getScopeController {

    static getScopebyProjectId(projectID) {
        return new Promise(async(resolve, reject) => {
            try {
                if (validator.isEmpty(projectID)) {
                    reject(new Error('Project Id is required'))
                } else {
                    // const scopesResult = await scopes.findAll({
                    //     where: {
                    //         project_id: projectID,

                    //     }
                    // });
                    const scopesResult = await sequelize.query(`SELECT * FROM scope_of_work WHERE project_id = ${projectID}`, { type: QueryTypes.SELECT });
                    if (scopesResult.length <= 0) {
                        resolve('No scopes listed in this particular project')
                    } else {
                        //console.log(scopesResult);
                        let output = scopesResult.reduce((prv, curr, index) => {

                            if (prv.length > 0) {

                                let index = prv.findIndex(value => value.scope_version_id == curr.scope_version_id)

                                if (index >= 0) {
                                    let userIndex = prv[index].user.findIndex(value => value.emp_id == curr.emp_id)
                                    if (userIndex == -1) {
                                        prv[index].user.push({
                                            "emp_id": curr.emp_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        })
                                    }
                                    let invIndex = prv[index].invoice.findIndex(value => value.invoice_id == curr.invoice_id)
                                        // console.log(invIndex, curr.invoice_id);
                                    if (invIndex == -1) {
                                        prv[index].invoice.push({
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        })

                                    }


                                } else {
                                    prv.push({
                                        ...curr,
                                        user: [{
                                            "emp_id": curr.emp_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        }],

                                        invoice: [{
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        }]
                                    })
                                }

                            } else {
                                prv = [{
                                    ...curr,
                                    user: [{
                                        "emp_id": curr.emp_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,


                                    }],

                                    invoice: [{
                                        "invoice_id": curr.invoice_id,
                                        "raised_hours": curr.raised_hours
                                    }]
                                }]
                            }

                            return prv
                        }, [])
                        resolve(scopesResult)
                    }

                }


            } catch (error) {
                reject(error)
            }
        })
    }



    static getScopebyscopeId(scopeID) {
        return new Promise(async(resolve, reject) => {
            try {
                if (validator.isEmpty(scopeID)) {
                    reject(new Error('Project Id is required'))
                } else {
                    // const scopesResult = await scopes.findAll({
                    //     where: {
                    //         project_id: projectID,

                    //     }
                    // });
                    const scopesIdResult = await sequelize.query(`SELECT DISTINCT scope_of_work.*, user_details.user_id,user_details.emp_id,user_details.user_first_name, user_details.user_last_name , scope_invoice_map.invoice_id, scope_invoice_map.raised_hours FROM scope_of_work LEFT JOIN scope_employee_map ON scope_of_work.id = scope_employee_map.scope_id LEFT JOIN user_details ON scope_employee_map.emp_id = user_details.emp_id LEFT JOIN scope_invoice_map ON scope_of_work.scope_name = scope_invoice_map.sub_scope_name WHERE scope_of_work.id  = ${scopeID}`, { type: QueryTypes.SELECT });

                    if (scopesIdResult.length <= 0) {
                        resolve('No scopes listed in this particular project')
                    } else {
                        //console.log(scopesResult);
                        let output = scopesIdResult.reduce((prv, curr, index) => {

                            if (prv.length > 0) {

                                let index = prv.findIndex(value => value.id == curr.id)

                                if (index >= 0) {
                                    let userIndex = prv[index].user.findIndex(value => value.emp_id == curr.emp_id)
                                    if (userIndex == -1) {
                                        prv[index].user.push({
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        })
                                    }
                                    let invIndex = prv[index].invoice.findIndex(value => value.invoice_id == curr.invoice_id)
                                        // console.log(invIndex, curr.invoice_id);
                                    if (invIndex == -1) {
                                        prv[index].invoice.push({
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        })

                                    }


                                } else {
                                    prv.push({
                                        ...curr,
                                        user: [{
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        }],

                                        invoice: [{
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        }]
                                    })
                                }

                            } else {
                                prv = [{
                                    ...curr,
                                    user: [{
                                        "emp_id": curr.emp_id,
                                        "user_id": curr.user_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,


                                    }],

                                    invoice: [{
                                        "invoice_id": curr.invoice_id,
                                        "raised_hours": curr.raised_hours
                                    }]
                                }]
                            }

                            return prv
                        }, [])
                        resolve(output)
                    }

                }


            } catch (error) {
                reject(error)
            }
        })
    }

    static getSubScopebyScopeId(scopeID) {
        return new Promise(async(resolve, reject) => {
            try {
                if (validator.isEmpty(scopeID)) {
                    reject(new Error('Project Id is required'))
                } else {
                    // const scopesResult = await scopes.findAll({
                    //     where: {
                    //         project_id: projectID,

                    //     }
                    // });
                    const scopesIdResult = await sequelize.query(`SELECT DISTINCT scope_of_work.id, scope_of_work.project_id,scope_of_work.scope_version,scope_version_table.*, user_details.user_id,user_details.emp_id,user_details.user_first_name, user_details.user_last_name , scope_invoice_map.invoice_id, scope_invoice_map.raised_hours FROM scope_of_work LEFT JOIN scope_version_table ON scope_of_work.id = scope_version_table.scope_id LEFT JOIN scope_employee_map ON scope_version_table.scope_version_id = scope_employee_map.scope_version_id LEFT JOIN user_details ON scope_employee_map.emp_id = user_details.emp_id LEFT JOIN scope_invoice_map ON scope_version_table.sub_scope_name = scope_invoice_map.sub_scope_name WHERE scope_version_table.scope_id  = ${scopeID}`, { type: QueryTypes.SELECT });

                    if (scopesIdResult.length <= 0) {
                        reject(new Error('No subscopes listed in this particular project'));
                    } else {
                        //console.log(scopesResult);
                        let output = scopesIdResult.reduce((prv, curr, index) => {

                            if (prv.length > 0) {

                                let index = prv.findIndex(value => value.scope_version_id == curr.scope_version_id)

                                if (index >= 0) {
                                    let userIndex = prv[index].user.findIndex(value => value.emp_id == curr.emp_id)
                                    if (userIndex == -1) {
                                        prv[index].user.push({
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        })
                                    }
                                    let invIndex = prv[index].invoice.findIndex(value => value.invoice_id == curr.invoice_id)
                                        // console.log(invIndex, curr.invoice_id);
                                    if (invIndex == -1) {
                                        prv[index].invoice.push({
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        })

                                    }


                                } else {
                                    prv.push({
                                        ...curr,
                                        user: [{
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        }],

                                        invoice: [{
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        }]
                                    })
                                }

                            } else {
                                prv = [{
                                    ...curr,
                                    user: [{
                                        "emp_id": curr.emp_id,
                                        "user_id": curr.user_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,


                                    }],

                                    invoice: [{
                                        "invoice_id": curr.invoice_id,
                                        "raised_hours": curr.raised_hours
                                    }]
                                }]
                            }

                            return prv
                        }, [])
                        resolve(output)
                    }

                }


            } catch (error) {
                reject(error)
            }
        })
    }



    static getScopebyScopeVersionID(scopeVersionID) {
        return new Promise(async(resolve, reject) => {
            try {
                if (validator.isEmpty(scopeVersionID)) {
                    reject(new Error('Scope ID Is empty'))
                } else {
                    // const scopesResult = await scopes.findAll({
                    //     where: {
                    //         project_id: projectID,

                    //     }
                    // });
                    const scopesIdResult = await sequelize.query(`SELECT DISTINCT scope_of_work.id, scope_of_work.project_id,scope_of_work.scope_version,scope_version_table.*, user_details.user_id,user_details.emp_id,user_details.user_first_name, user_details.user_last_name , scope_invoice_map.invoice_id, scope_invoice_map.raised_hours FROM scope_of_work LEFT JOIN scope_version_table ON scope_of_work.id = scope_version_table.scope_id LEFT JOIN scope_employee_map ON scope_version_table.scope_version_id = scope_employee_map.scope_version_id LEFT JOIN user_details ON scope_employee_map.emp_id = user_details.emp_id LEFT JOIN scope_invoice_map ON scope_version_table.sub_scope_name = scope_invoice_map.sub_scope_name WHERE scope_version_table.scope_version_id  = ${scopeVersionID}`, { type: QueryTypes.SELECT });

                    if (scopesIdResult.length <= 0) {
                        reject(new Error('No scope found with this ID'));
                    } else {
                        //console.log(scopesResult);
                        let output = scopesIdResult.reduce((prv, curr, index) => {

                            if (prv.length > 0) {

                                let index = prv.findIndex(value => value.scope_version_id == curr.scope_version_id)

                                if (index >= 0) {
                                    let userIndex = prv[index].user.findIndex(value => value.emp_id == curr.emp_id)
                                    if (userIndex == -1) {
                                        prv[index].user.push({
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        })
                                    }
                                    let invIndex = prv[index].invoice.findIndex(value => value.invoice_id == curr.invoice_id)
                                        // console.log(invIndex, curr.invoice_id);
                                    if (invIndex == -1) {
                                        prv[index].invoice.push({
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        })

                                    }


                                } else {
                                    prv.push({
                                        ...curr,
                                        user: [{
                                            "emp_id": curr.emp_id,
                                            "user_id": curr.user_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,

                                        }],

                                        invoice: [{
                                            "invoice_id": curr.invoice_id,
                                            "raised_hours": curr.raised_hours
                                        }]
                                    })
                                }

                            } else {
                                prv = [{
                                    ...curr,
                                    user: [{
                                        "emp_id": curr.emp_id,
                                        "user_id": curr.user_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,


                                    }],

                                    invoice: [{
                                        "invoice_id": curr.invoice_id,
                                        "raised_hours": curr.raised_hours
                                    }]
                                }]
                            }

                            return prv
                        }, [])
                        resolve(output)
                    }

                }


            } catch (error) {
                reject(error)
            }
        })
    }
}

module.exports = getScopeController