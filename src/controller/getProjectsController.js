const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes } = require('sequelize');
const { sequelize } = require("../models/index");
const { Op } = require("sequelize");
const projects = scopeDatabase.projects

class getProjectsController {

    static getProjects() {
        return new Promise(async(resolve, reject) => {
            try {

                const projectResult = await projects.findAll();
                if (projectResult.length <= 0) {
                    reject(new Error('No projects'))
                } else {
                    resolve(projectResult)
                }




            } catch (error) {
                reject(error)
            }
        })
    }

    static getProjectsbyUserID(userID) {
        return new Promise(async(resolve, reject) => {
            try {


                const projectResult = await projects.findAll({
                    where: {
                        project_lead: {
                            [Op.in]: [parseInt(userID)]
                        }
                    }
                });
                if (projectResult.length <= 0) {
                    reject(new Error('No projects'))
                } else {
                    resolve(projectResult)
                }




            } catch (error) {
                reject(error)
            }
        })
    }

}

module.exports = getProjectsController