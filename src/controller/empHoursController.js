const scopeDatabase = require("../models/index");
const { sequelize, scope } = require("../models/index");
const { QueryTypes } = require('sequelize');
const { Op } = require("sequelize");
const moment = require('moment');
const { response } = require("express");
const scopeEmpHours = scopeDatabase.scopeEmpHours
class empHoursController {

    static insertEmphours(empHoursbody) {
        return new Promise(async(resolve, reject) => {
            try {
                const existingDate = await scopeEmpHours.findAll({
                    where: {
                        [Op.and]: [
                            { date: empHoursbody.date },
                            { emp_id: empHoursbody.emp_id }
                        ]
                    }
                });
                if (existingDate.length > 0) {
                    reject(new Error('This date entry has already been entered.'))
                } else {
                    const insertEmpHoursResult = await scopeEmpHours.create(empHoursbody);
                    resolve(insertEmpHoursResult);
                }


            } catch (error) {
                reject(error)
            }
        })
    }

    static insertEmphoursbyTL(empHoursbodyTL) {
        return new Promise(async(resolve, reject) => {
            try {
                const finalResult = []
                empHoursbodyTL.map(async(empItem) => {

                    const existingDate = await sequelize.query(`SELECT * FROM scope_employee_hours WHERE scope_employee_hours.date = '${empItem.date}' AND scope_employee_hours.emp_id = ${empItem.emp_id}`, { type: QueryTypes.SELECT })
                        // console.log(existingDate);
                    if (existingDate.length > 0) {
                        //reject(new Error('This date entry has already been entered for this empID ' + empItem.emp_id))
                        //console.log('entry exists for this id' + empItem.emp_id)
                        const updateHours = await sequelize.query(`UPDATE scope_employee_hours SET hours_spent=${empItem.hours_spent} WHERE scope_employee_hours.date='${empItem.date}' AND scope_employee_hours.emp_id = ${empItem.emp_id}`, { type: QueryTypes.UPDATE })
                        finalResult.push(updateHours);
                    } else {
                        const insertEmpHoursResult = await scopeEmpHours.create(empItem);
                        finalResult.push(insertEmpHoursResult);
                    }
                })
                resolve(finalResult);

            } catch (error) {
                reject(error)
            }
        })
    }

    static getempHoursbyEmpId(empQuery) {
        return new Promise(async(resolve, reject) => {
            try {
                const getEmpHoursDetails = await sequelize.query(`SELECT scope_employee_hours.emp_id,user_details.user_first_name,scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description as subscope_description,scope_employee_hours.date, scope_employee_hours.hours_spent FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN user_details ON user_details.emp_id = scope_employee_hours.emp_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id WHERE scope_employee_hours.project_id = ${empQuery.projectid} AND scope_employee_hours.emp_id = ${empQuery.empid}`, { type: QueryTypes.SELECT });

                if (getEmpHoursDetails.length <= 0) {
                    resolve('No Hours listed in this Project')
                } else {
                    resolve(getEmpHoursDetails);
                }



            } catch (error) {
                reject(error)
            }
        })
    }

    static getempHoursbyTlId(tlquery) {
        return new Promise(async(resolve, reject) => {
            try {
                const empIDArray = [];
                const empunderTlResult = await sequelize.query(`SELECT tl_user_assignment_mapping.user_id, user_details.user_first_name, user_details.user_last_name FROM tl_user_assignment_mapping LEFT JOIN user_details ON tl_user_assignment_mapping.user_id = user_details.user_id WHERE tl_user_assignment_mapping.tl_id = ${parseInt(tlquery.tlid)}`)
                empunderTlResult[0].map((empItem) => {
                        //console.log(empItem.user_id)
                        empIDArray.push(empItem.user_id);
                    })
                    //console.log(typeof empIDArray)
                const empArrayreplaced = JSON.stringify(empIDArray).replace('[', '(');
                const finalEmpArray = empArrayreplaced.replace(']', ')');
                // console.log(finalEmpArray);
                const getEmpQuery = `SELECT user_details.emp_id,user_details.user_first_name, user_details.user_last_name, scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description AS sub_scope_description,scope_employee_hours.hours_spent,scope_employee_hours.date FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id LEFT JOIN user_details ON user_details.emp_id = scope_employee_hours.emp_id WHERE scope_employee_hours.project_id = ${parseInt(tlquery.projectid)} AND scope_employee_hours.scope_id=${parseInt(tlquery.scopeid)} AND scope_employee_hours.date BETWEEN '${tlquery.startdate}' AND '${tlquery.enddate}' AND scope_employee_hours.emp_id IN ${finalEmpArray}`


                const getEmpbulkResult = await sequelize.query(getEmpQuery, { type: QueryTypes.SELECT })


                let output = getEmpbulkResult.reduce((prv, curr, index) => {
                    //console.log(prv)
                    if (prv.length > 0) {
                        //console.log(curr);
                        let scopeIndex = prv.findIndex(value => value.scope_name == curr.scope_name)
                            //console.log(scopeIndex)

                        if (scopeIndex >= 0) {
                            let dateIndex = prv[scopeIndex].scopeHours.findIndex(value => value.date == curr.date);
                            // console.log(dateIndex);

                            if (dateIndex !== -1) {
                                //console.log(prv[dateIndex].scopeHours);
                                prv[scopeIndex].scopeHours[dateIndex].data.push({
                                    "emp_id": curr.emp_id,
                                    "user_first_name": curr.user_first_name,
                                    "user_last_name": curr.user_last_name,
                                    "hours_spent": curr.hours_spent

                                })
                            } else {
                                prv[scopeIndex].scopeHours.push(

                                    {
                                        "date": curr.date,
                                        "data": [{
                                                "emp_id": curr.emp_id,
                                                "user_first_name": curr.user_first_name,
                                                "user_last_name": curr.user_last_name,
                                                "hours_spent": curr.hours_spent
                                            }

                                        ]
                                    }
                                )


                            }

                        } else {
                            prv.push({
                                "scope_name": curr.scope_name,
                                "scope_description": curr.description,
                                scopeHours: [{
                                    "date": curr.date,
                                    "data": [{
                                            "emp_id": curr.emp_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,
                                            "hours_spent": curr.hours_spent
                                        }

                                    ]
                                }],


                            })
                        }

                    } else {
                        prv = [{
                            "scope_name": curr.scope_name,
                            "scope_description": curr.description,
                            scopeHours: [{
                                "date": curr.date,
                                "data": [{
                                        "emp_id": curr.emp_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,
                                        "hours_spent": curr.hours_spent
                                    }

                                ]
                            }],


                        }]
                    }


                    return prv
                }, [])
                resolve(output)


            } catch (error) {
                reject(error)
            }
        })
    }

    static getEmpHoursbyIDforUpdate(empQuery) {
        return new Promise(async(resolve, reject) => {
            try {
                const getEmpHoursDetailsforUpdate = await sequelize.query(`SELECT user_details.user_first_name, user_details.user_last_name, user_details.emp_id, scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description as subscope_description,scope_employee_hours.date, scope_employee_hours.hours_spent FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN user_details ON user_details.emp_id=scope_employee_hours.emp_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id WHERE scope_employee_hours.date = '${empQuery.date}' AND scope_employee_hours.emp_id = ${empQuery.empid}`, { type: QueryTypes.SELECT });

                if (getEmpHoursDetailsforUpdate.length <= 0) {
                    resolve('No Hours listed in this Project')
                } else {
                    resolve(getEmpHoursDetailsforUpdate);
                }



            } catch (error) {
                reject(error)
            }
        })
    }

    static updateEmpHoursbyempID(empQuery) {
        return new Promise(async(resolve, reject) => {
            try {
                const updateEmpResult = await sequelize.query(`UPDATE scope_employee_hours SET hours_spent= ${empQuery.hours} WHERE scope_employee_hours.emp_id = ${empQuery.empid} AND scope_employee_hours.date = '${empQuery.date}';`);


                resolve(updateEmpResult);




            } catch (error) {
                reject(error)
            }
        })
    }

    static getReportEmpHours(empQuery) {
        return new Promise(async(resolve, reject) => {
            try {
                if (empQuery.type === 'daily') {
                    const empIDArray = [];
                    const empunderTlResult = await sequelize.query(`SELECT tl_user_assignment_mapping.user_id, user_details.user_first_name, user_details.user_last_name FROM tl_user_assignment_mapping LEFT JOIN user_details ON tl_user_assignment_mapping.user_id = user_details.user_id WHERE tl_user_assignment_mapping.tl_id = ${parseInt(empQuery.tlid)}`)
                    empunderTlResult[0].map((empItem) => {
                            //console.log(empItem.user_id)
                            empIDArray.push(empItem.user_id);
                        })
                        //console.log(typeof empIDArray)
                    const empArrayreplaced = JSON.stringify(empIDArray).replace('[', '(');
                    const finalEmpArray = empArrayreplaced.replace(']', ')');
                    const getEmpQuery = `SELECT user_details.emp_id,user_details.user_first_name, user_details.user_last_name, scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description AS sub_scope_description,scope_employee_hours.hours_spent,scope_employee_hours.date FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id LEFT JOIN user_details ON user_details.emp_id = scope_employee_hours.emp_id WHERE scope_employee_hours.project_id = ${parseInt(empQuery.projectid)} AND scope_employee_hours.scope_id=${parseInt(empQuery.scopeid)} AND scope_employee_hours.date BETWEEN '${empQuery.startdate}' AND '${empQuery.enddate}' AND scope_employee_hours.emp_id IN ${finalEmpArray}`

                    const getTotalCumilativeHours = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_cumilative_hours FROM scope_employee_hours WHERE scope_employee_hours.date BETWEEN '${empQuery.startdate}' AND '${empQuery.enddate}'`);
                    const cumilativeHoursResult = getTotalCumilativeHours[0][0].total_cumilative_hours;
                    const getScopeStartDate = await sequelize.query(`SELECT scope_of_work.start_date FROM scope_of_work WHERE scope_of_work.id = ${parseInt(empQuery.scopeid)}`)
                    const scopeStartDate = getScopeStartDate[0][0].start_date;
                    const getScopehourstillDate = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_scope_hours FROM scope_employee_hours WHERE scope_employee_hours.date >= '${scopeStartDate}' and scope_employee_hours.scope_id = ${parseInt(empQuery.scopeid)}`)
                    const finalScopeHourstillDate = getScopehourstillDate[0][0].total_scope_hours
                    const getEmpbulkResult = await sequelize.query(getEmpQuery, { type: QueryTypes.SELECT })

                    let output = getEmpbulkResult.reduce((prv, curr, index) => {
                        //console.log(prv)
                        if (prv.length > 0) {
                            //console.log(curr);
                            let scopeIndex = prv.findIndex(value => value.scope_name == curr.scope_name)
                                //console.log(scopeIndex)

                            if (scopeIndex >= 0) {
                                let dateIndex = prv[scopeIndex].scopeHours.findIndex(value => value.date == curr.date);
                                // console.log(dateIndex);

                                if (dateIndex !== -1) {
                                    //console.log(prv[dateIndex].scopeHours);
                                    prv[scopeIndex].scopeHours[dateIndex].data.push({
                                        "emp_id": curr.emp_id,
                                        "user_first_name": curr.user_first_name,
                                        "user_last_name": curr.user_last_name,
                                        "hours_spent": curr.hours_spent

                                    })
                                } else {
                                    prv[scopeIndex].scopeHours.push(

                                        {
                                            "date": curr.date,
                                            "data": [{
                                                    "emp_id": curr.emp_id,
                                                    "user_first_name": curr.user_first_name,
                                                    "user_last_name": curr.user_last_name,
                                                    "hours_spent": curr.hours_spent
                                                }

                                            ]
                                        }
                                    )


                                }

                            } else {
                                prv.push({
                                    "scope_name": curr.scope_name,
                                    "scope_description": curr.description,
                                    scopeHours: [{
                                        "date": curr.date,
                                        "data": [{
                                                "emp_id": curr.emp_id,
                                                "user_first_name": curr.user_first_name,
                                                "user_last_name": curr.user_last_name,
                                                "hours_spent": curr.hours_spent
                                            }

                                        ]
                                    }],


                                })
                            }

                        } else {
                            prv = [{
                                "scope_name": curr.scope_name,
                                "scope_description": curr.description,
                                scopeHours: [{
                                    "date": curr.date,
                                    "data": [{
                                            "emp_id": curr.emp_id,
                                            "user_first_name": curr.user_first_name,
                                            "user_last_name": curr.user_last_name,
                                            "hours_spent": curr.hours_spent
                                        }

                                    ]
                                }],


                            }]
                        }


                        return prv
                    }, [])

                    resolve({ type: 'daily', totals: { total_cumilative_hours: cumilativeHoursResult, total_scope_hours: finalScopeHourstillDate, total_offshore_hours: '0' }, result: output });
                } else if (empQuery.type === "weekly") {
                    const empIDArray = [];
                    const empHours = [];
                    const weekRange = [];
                    const empunderTlResult = await sequelize.query(`SELECT tl_user_assignment_mapping.user_id, user_details.user_first_name, user_details.user_last_name FROM tl_user_assignment_mapping LEFT JOIN user_details ON tl_user_assignment_mapping.user_id = user_details.user_id WHERE tl_user_assignment_mapping.tl_id = ${parseInt(empQuery.tlid)}`)

                    const getTotalCumilativeHours = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_cumilative_hours FROM scope_employee_hours WHERE scope_employee_hours.date BETWEEN '${empQuery.startdate}' AND '${empQuery.enddate}'`);
                    const cumilativeHoursResult = getTotalCumilativeHours[0][0].total_cumilative_hours;
                    const getScopeStartDate = await sequelize.query(`SELECT scope_of_work.start_date FROM scope_of_work WHERE scope_of_work.id = ${parseInt(empQuery.scopeid)}`)
                    const scopeStartDate = getScopeStartDate[0][0].start_date;
                    const getScopehourstillDate = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_scope_hours FROM scope_employee_hours WHERE scope_employee_hours.date >= '${scopeStartDate}' and scope_employee_hours.scope_id = ${parseInt(empQuery.scopeid)}`)
                    const finalScopeHourstillDate = getScopehourstillDate[0][0].total_scope_hours
                    empunderTlResult[0].map((empItem) => {
                        //console.log(empItem.user_id)
                        empIDArray.push(empItem.user_id);
                    })
                    var currDay = moment(empQuery.startdate, "YYYY-MM-DD");
                    var endDay = moment(empQuery.enddate, "YYYY-MM-DD");

                    var diff = endDay.diff(currDay, 'weeks');
                    // console.log(diff);
                    for (var i = 0; i <= diff; i++) {
                        //console.log("current before shift " + moment(currDay).format("YYYY-MM-DD"));
                        if (currDay.weekday() === 6) {
                            currDay.add(2, 'days')
                        } else if (currDay.weekday() === 0) {
                            currDay.add(1, 'days')
                        }
                        //console.log("current after shift " + moment(currDay).format("YYYY-MM-DD"));
                        var currentDateString = moment(currDay).format("YYYY-MM-DD");
                        var initialendDate = currDay.add(6, 'days')
                            //console.log("initail before shift " + moment(initialendDate).format("YYYY-MM-DD"));
                        if (initialendDate.weekday() === 0) {
                            initialendDate.subtract(2, 'days')
                        } else if (initialendDate.weekday() === 6) {
                            initialendDate.subtract(1, 'days')
                        }
                        //console.log("initial after shift " + moment(initialendDate).format("YYYY-MM-DD"));
                        //console.log("initial " + moment(currDay).format("YYYY-MM-DD"));
                        var endDateString = moment(endDay).format("YYYY-MM-DD");
                        var InitialDateString = moment(initialendDate).format("YYYY-MM-DD");
                        if (initialendDate <= endDay) {
                            //console.log(currentDateString + '--' + InitialDateString);
                            weekRange.push({ weekStartDate: currentDateString, weekEndDate: InitialDateString });
                            if (endDay.diff(initialendDate, 'days') < 7) {
                                weekRange.push({ weekStartDate: InitialDateString, weekEndDate: endDateString });
                                console.log(InitialDateString + '-->>' + endDateString);
                                console.log('one week without 7 days. Not valid')
                            }
                            currDay.add(1, 'days')

                        } else {

                            console.log('end date reached');
                        }

                    }
                    // console.log(weekRange);
                    const finalResult = await Promise.all(weekRange.map(async(date) => {
                        const EmpHoursWeekResult = await this.getEmpHoursbyWeek(date.weekStartDate, date.weekEndDate, 13, empQuery.projectid, empQuery.scopeid, empIDArray);

                        var feed = { fromDate: date.weekStartDate, toDate: date.weekEndDate, data: EmpHoursWeekResult }
                        return feed;
                    }))
                    resolve({ type: 'weekly', totals: { total_cumilative_hours: cumilativeHoursResult, total_scope_hours: finalScopeHourstillDate, total_offshore_hours: '0' }, result: [{ scopeHours: finalResult }] })

                } else if (empQuery.type === "monthly") {
                    const empIDArray = [];
                    const empHours = [];
                    const monthRange = [];
                    const empunderTlResult = await sequelize.query(`SELECT tl_user_assignment_mapping.user_id, user_details.user_first_name, user_details.user_last_name FROM tl_user_assignment_mapping LEFT JOIN user_details ON tl_user_assignment_mapping.user_id = user_details.user_id WHERE tl_user_assignment_mapping.tl_id = ${parseInt(empQuery.tlid)}`)

                    const getTotalCumilativeHours = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_cumilative_hours FROM scope_employee_hours WHERE scope_employee_hours.date BETWEEN '${empQuery.startdate}' AND '${empQuery.enddate}'`);
                    const cumilativeHoursResult = getTotalCumilativeHours[0][0].total_cumilative_hours;
                    const getScopeStartDate = await sequelize.query(`SELECT scope_of_work.start_date FROM scope_of_work WHERE scope_of_work.id = ${parseInt(empQuery.scopeid)}`)
                    const scopeStartDate = getScopeStartDate[0][0].start_date;
                    const getScopehourstillDate = await sequelize.query(`SELECT SUM(scope_employee_hours.hours_spent) as total_scope_hours FROM scope_employee_hours WHERE scope_employee_hours.date >= '${scopeStartDate}' and scope_employee_hours.scope_id = ${parseInt(empQuery.scopeid)}`)
                    const finalScopeHourstillDate = getScopehourstillDate[0][0].total_scope_hours
                    empunderTlResult[0].map((empItem) => {
                        //console.log(empItem.user_id)
                        empIDArray.push(empItem.user_id);
                    })
                    var currDay = moment(empQuery.startdate, "YYYY-MM-DD");
                    var endDay = moment(empQuery.enddate, "YYYY-MM-DD");

                    var diff = endDay.diff(currDay, 'months');
                    //console.log(diff);
                    for (var i = 0; i <= diff; i++) {
                        //console.log("current before shift " + moment(currDay).format("YYYY-MM-DD"));
                        const startDateofMonth = moment(currDay).startOf('month').format("YYYY-MM-DD");
                        const endDateofMonth = moment(currDay).endOf('month').format("YYYY-MM-DD");
                        monthRange.push({ monthStartDate: startDateofMonth, monthEndDate: endDateofMonth });
                        //console.log("current after shift " + moment(currDay).format("YYYY-MM-DD"));
                        var currentDateString = moment(currDay).format("YYYY-MM-DD");
                        var initialendDate = currDay.add(32, 'days')


                        var endDateString = moment(endDay).format("YYYY-MM-DD");
                        var InitialDateString = moment(initialendDate).format("YYYY-MM-DD");
                        if (initialendDate <= endDay) {
                            //console.log(currentDateString + '--' + InitialDateString);
                            // monthRange.push({ weekStartDate: currentDateString, weekEndDate: InitialDateString });
                            if (endDay.diff(currentDateString, 'days') < 30) {
                                //monthRange.push({ weekStartDate: InitialDateString, weekEndDate: endDateString });
                                console.log(currentDateString + '-->>' + endDateString);
                                console.log('one month without 30 days. Not valid')
                            }

                        } else {

                            console.log('end date reached');
                        }

                    }
                    // console.log(weekRange);
                    const finalResult = await Promise.all(monthRange.map(async(date) => {
                        const EmpHoursWeekResult = await this.getEmpHoursbyMonth(date.monthStartDate, date.monthEndDate, 13, empQuery.projectid, empQuery.scopeid, empIDArray);

                        var feed = { fromDate: date.monthStartDate, toDate: date.monthEndDate, data: EmpHoursWeekResult }
                        return feed;
                    }))
                    resolve({ type: 'monthly', totals: { total_cumilative_hours: cumilativeHoursResult, total_scope_hours: finalScopeHourstillDate, total_offshore_hours: '0' }, result: [{ scopeHours: finalResult }] })
                }

            } catch (error) {
                reject(error)
            }
        })
    }

    static async getEmpHoursbyWeek(startDate, endDate, empID, projectID, scopeID, empIDArray) {
        const result = await Promise.all(empIDArray.map(async(elementID) => {
            const getEmpQuery = `SELECT user_details.emp_id,user_details.user_first_name, user_details.user_last_name, scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description AS sub_scope_description, SUM(scope_employee_hours.hours_spent) as hours_this_week FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id LEFT JOIN user_details ON user_details.emp_id = scope_employee_hours.emp_id WHERE scope_employee_hours.project_id = ${parseInt(projectID)} AND scope_employee_hours.scope_id=${parseInt(scopeID)} AND scope_employee_hours.date BETWEEN '${startDate}' AND '${endDate}' AND scope_employee_hours.emp_id = ${elementID}`;
            const getEmpbulkResult = await sequelize.query(getEmpQuery, { type: QueryTypes.SELECT })
            const userDetails = await sequelize.query(`SELECT user_details.user_first_name, user_details.user_last_name FROM user_details WHERE user_details.emp_id = ${elementID}`, { type: QueryTypes.SELECT })
            const scopeDetails = await sequelize.query(`SELECT scope_of_work.scope_name, scope_of_work.description AS scope_description FROM scope_of_work WHERE scope_of_work.id = ${scopeID}`, { type: QueryTypes.SELECT })
            if (getEmpbulkResult[0].emp_id === null) {
                //    console.log('do not return');
                getEmpbulkResult[0].emp_id = elementID;
                getEmpbulkResult[0].user_first_name = userDetails[0].user_first_name;
                getEmpbulkResult[0].user_last_name = userDetails[0].user_last_name;
                getEmpbulkResult[0].scope_name = scopeDetails[0].scope_name;
                getEmpbulkResult[0].description = scopeDetails[0].scope_description;
                delete getEmpbulkResult[0].sub_scope_name
                delete getEmpbulkResult[0].sub_scope_description
                getEmpbulkResult[0].hours_this_week = '0';
                return getEmpbulkResult
            } else {
                return getEmpbulkResult
            }

        }));

        return result;


    }

    static async getEmpHoursbyMonth(startDate, endDate, empID, projectID, scopeID, empIDArray) {
        const result = await Promise.all(empIDArray.map(async(elementID) => {
            const getEmpQuery = `SELECT user_details.emp_id,user_details.user_first_name, user_details.user_last_name, scope_of_work.scope_name, scope_of_work.description, scope_version_table.sub_scope_name, scope_version_table.description AS sub_scope_description, SUM(scope_employee_hours.hours_spent) as hours_this_month FROM scope_employee_hours LEFT JOIN scope_of_work ON scope_of_work.id = scope_employee_hours.scope_id LEFT JOIN scope_version_table ON scope_version_table.scope_version_id = scope_employee_hours.scope_version_id LEFT JOIN user_details ON user_details.emp_id = scope_employee_hours.emp_id WHERE scope_employee_hours.project_id = ${parseInt(projectID)} AND scope_employee_hours.scope_id=${parseInt(scopeID)} AND scope_employee_hours.date BETWEEN '${startDate}' AND '${endDate}' AND scope_employee_hours.emp_id = ${elementID}`;
            const getEmpbulkResult = await sequelize.query(getEmpQuery, { type: QueryTypes.SELECT })
            const userDetails = await sequelize.query(`SELECT user_details.user_first_name, user_details.user_last_name FROM user_details WHERE user_details.emp_id = ${elementID}`, { type: QueryTypes.SELECT })
            const scopeDetails = await sequelize.query(`SELECT scope_of_work.scope_name, scope_of_work.description AS scope_description FROM scope_of_work WHERE scope_of_work.id = ${scopeID}`, { type: QueryTypes.SELECT })
            if (getEmpbulkResult[0].emp_id === null) {
                //    console.log('do not return');
                getEmpbulkResult[0].emp_id = elementID;
                getEmpbulkResult[0].user_first_name = userDetails[0].user_first_name;
                getEmpbulkResult[0].user_last_name = userDetails[0].user_last_name;
                getEmpbulkResult[0].scope_name = scopeDetails[0].scope_name;
                getEmpbulkResult[0].description = scopeDetails[0].scope_description;
                delete getEmpbulkResult[0].sub_scope_name
                delete getEmpbulkResult[0].sub_scope_description
                getEmpbulkResult[0].hours_this_month = '0';
                return getEmpbulkResult
            } else {
                return getEmpbulkResult
            }

        }));

        return result;


    }
}

module.exports = empHoursController