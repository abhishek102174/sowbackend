const scopeDatabase = require("../models/index");
const validator = require('validator');
const { QueryTypes, Op } = require('sequelize');
const { sequelize, scope } = require("../models/index");
const scopes = scopeDatabase.scope
const scopeVersion = scopeDatabase.scopeVersion
const scopeEmp = scopeDatabase.scopeEmp
const scopeInv = scopeDatabase.scopeInv
const moment = require('moment');
class invoiceController {

    static insertInvoice(invoiceBody) {
        return new Promise(async(resolve, reject) => {
            try {
                // console.log(existingScopeResult);

                const invoiceBodytoInsert = {
                    sub_scope_name: invoiceBody.sub_scope_name,
                    invoice_id: invoiceBody.invoice_id,
                    raised_hours: invoiceBody.raised_hours
                }
                const insertInvResult = await scopeInv.create(invoiceBodytoInsert);
                resolve(insertInvResult)

            } catch (error) {
                console.log(error);
                reject(error)
            }


            // const scopeVersionBody = {
            //     scope_id: insertScopeResult.id,
            //     scope_name: scopeBody.scope_name,
            //     start_date: scopeBody.start_date,
            //     end_date: scopeBody.end_date,
            //     total_number_of_hours: scopeBody.total_number_of_hours,
            //     description: scopeBody.description,
            //     created_by: scopeBody.created_by,
            //     created_at: moment().format('YYYY-MM-DD')

            // }

            // const insertScopeVersionResult = await scopeVersion.create(scopeVersionBody);
            // var scopeEmparray = JSON.parse("[" + scopeBody.emp_id + "]");
            // //console.log(scopeEmparray);
            // scopeEmparray.map(async(empID) => {
            //     try {
            //         const scopeEmpBody = {
            //             scope_version_id: insertScopeVersionResult.scope_version_id,
            //             emp_id: empID,
            //             status: 1

            //         }

            //         const insertScopeEmpResult = await scopeEmp.create(scopeEmpBody);

            //         //console.log(insertScopeEmpResult);
            //     } catch (error) {
            //         console.log(error);
            //         reject(error)
            //     }

            // })

            // const scopeInvBody = {
            //     scope_version_id: insertScopeVersionResult.scope_version_id,
            //     invoice_id: scopeBody.invoice_id,
            //     raised_hours: scopeBody.raised_hours
            // }
            // const insertScopeInvResult = await scopeInv.create(scopeInvBody);
            // console.log(insertScopeInvResult);







        })
    }




}

module.exports = invoiceController