const dotenv = require('dotenv').config();

let config = {
    development: {
        url: 'http://localhost',
        //mySQL connection settings
        database: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            database: process.env.DB_NAME,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            connectionLimit: process.env.DB_CONNECTION_LIMIT || 10,
            queueLimit: process.env.DB_QUEUE_LIMIT || 100,
        },
        //server details
        server: {
            host: 'localhost',
            port: '3000'
        }

    },
    production: {
        //url to be used in link generation
        url: 'http://localhost',
        database: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            database: process.env.DB_DATABASE,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD
        },
        //server details
        server: {
            host: '127.0.0.1',
            port: '3000'
        }
    },
    auth: {
        jwt_secret: process.env.JWT_SECRET_KEY || 'c++v!!!l000)()ddd()s@S(j(k(l(m()a)j)m',
        jwt_expiresin: process.env.JWT_EXPIRES_IN || '1d',
        jwt_auth_header_name: 'authorization',
        jwt_auth_value_prefix: 'Bearer',
        jwt_auth_data: process.env.JWT_DATA_KEY
    }
};
module.exports = config;