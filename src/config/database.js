const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env];
const mysql = require('mysql');
const util = require('util');

const connection = mysql.createConnection(config.database);
const query = util.promisify(connection.query).bind(connection);

const executeQuery = async function(nativeQuery){
    try {
        const records = await query(nativeQuery);
        return records;
    } catch(e){
        console.log(e);
    }
        // } finally{
    //     connection.end();
    // }
};

module.exports = executeQuery;