const { getUserByIdQuery, createUserQuery, getUserByEmailQuery } = require('../native-queries/userQueries');
const executeQuery = require('../config/database');

const {
    ResourceNotFoundError,
    ConflictError,
    InternalError,
    BadRequest,
    UnauthorizedError
} = require('../helpers/errorHandler');

async function getUserById(userId){
    try {
       const records = await executeQuery(getUserByIdQuery(userId));
       if(!records.length){
        throw Error("user not present");
       }
       return records;
    } catch(e){
        console.log(e);

    }
}

async function updateUser(userId){
    try {
       const records = await executeQuery(getUserByIdQuery(userId));
       if(!records.length){
        throw Error("user not present");
       }
       return records;
    } catch(e){
        console.log(e);

    }
}

async function createUserService(UserInfo){
    // try {
    const checkUserExist = await executeQuery(getUserByEmailQuery(UserInfo.email));
    if(checkUserExist.length){
        throw new ConflictError(`User with same email already present`);
    }
    console.log(`checkUserExist ${JSON.stringify(checkUserExist)}`);
    const records = await executeQuery(createUserQuery(UserInfo));
    console.log(records);
    return records;
    // } catch(e){
    //     console.log(e);
    // }
}

module.exports = {
    getUserById,
    updateUser,
    createUserService
}