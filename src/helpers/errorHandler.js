class DomainError extends Error {
    constructor(errorMessage, errorCode) {
        super(errorMessage);
        this.statusCode = errorCode;
    }
}

class ResourceNotFoundError extends DomainError {
    constructor(errorMessage, statusCode) {
        super(errorMessage);
        this.statusCode = 404;
    }
}

class ConflictError extends DomainError {
    constructor(errorMessage, statusCode) {
        super(errorMessage);
        this.statusCode = statusCode;
    }
}

class BadRequest extends DomainError {
    constructor(errorMessage, statusCode) {
        super(errorMessage);
        this.statusCode = 400;
    }
}

class InternalError extends DomainError {
    constructor(errorMessage, statusCode) {
        super(errorMessage);
        this.statusCode = 500;
    }
}

class UnauthorizedError extends DomainError {
    constructor(message, statusCode) {
        super(message);
        this.statusCode = 401;
    }
}

module.exports = {
    DomainError,
    ResourceNotFoundError,
    ConflictError,
    InternalError,
    BadRequest,
    UnauthorizedError
  };