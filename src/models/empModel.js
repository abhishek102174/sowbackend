let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {

    let EmpDetails = () => {
        return sequelize.define('user_details', {
            user_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            user_name: {
                type: DataTypes.STRING(255),

            },
            user_first_name: {
                type: DataTypes.STRING(255),

            },
            user_last_name: {
                type: DataTypes.STRING(255),

            },
            emp_id: {
                type: DataTypes.STRING(255),

            }
        })
    }



    return { EmpDetails }


};