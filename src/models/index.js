const { development } = require('../config/config')
const Sequelize = require('sequelize')

const sequelize = new Sequelize({
    dialect: 'mysql',
    host: development.database.host,
    username: development.database.user,
    password: development.database.password,
    database: development.database.database,
    port: development.database.port,
    define: {
        freezeTableName: true,
        timestamps: false,
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: false
});

const database = {};

database.Sequelize = Sequelize;
database.sequelize = sequelize;

// sequelize
//     .authenticate()
//     .then(() => {
//         console.log('Connection has been established successfully.');
//     })
//     .catch(err => {
//         console.error('Unable to connect to the database:', err);
//     });

database.scope = require("./scopeModel")(sequelize, Sequelize);
database.projects = require("./projectModel")(sequelize, Sequelize)
database.scopeVersion = require("./scopeVersionModel")(sequelize, Sequelize)
database.scopeEmp = require("./scopeEmpModel")(sequelize, Sequelize)
database.getEmp = require('./empModel')(sequelize, Sequelize)
database.scopeNotifictaionEmail = require('./scopeNotificationModel')(sequelize, Sequelize)
database.scopeInv = require('./scopeInvoiceModel')(sequelize, Sequelize)
database.scopeEmpHours = require('./scopeEmpHoursModel')(sequelize, Sequelize)
module.exports = database;