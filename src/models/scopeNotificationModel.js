let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeNotficationEmail = sequelize.define('scope_notification_master', {
        email_notification_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        scope_version_id: {
            type: DataTypes.INTEGER(20),

        },
        scope_notified_emails: {
            type: DataTypes.STRING(255),

        },
        email_sent_date: {
            type: DataTypes.DATE(20),

        }

    })
    return ScopeNotficationEmail;
};