let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeInvTable = sequelize.define('scope_invoice_map', {
        scope_invoice_id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true
        },
        sub_scope_name: {
            type: DataTypes.STRING(255),

        },
        invoice_id: {
            type: DataTypes.INTEGER(20),

        },
        raised_hours: {
            type: DataTypes.INTEGER(20),

        },




    }, {
        freezeTableName: true

    })
    return ScopeInvTable;
};