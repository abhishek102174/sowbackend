let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeEmpTable = sequelize.define('scope_employee_map', {
        scope_version_id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true
        },
        scope_id: {
            type: DataTypes.INTEGER(20),
        },
        emp_id: {
            type: DataTypes.INTEGER(20),

        },
        status: {
            type: DataTypes.INTEGER(20),

        },




    }, {
        freezeTableName: true

    })
    return ScopeEmpTable;
};