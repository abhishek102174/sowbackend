let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeEmpHours = sequelize.define('scope_employee_hours', {
        id: {
            type: DataTypes.INTEGER(20),
            primaryKey: true
        },
        emp_id: {
            type: DataTypes.STRING(255),

        },
        project_id: {
            type: DataTypes.INTEGER(20),

        },
        scope_id: {
            type: DataTypes.INTEGER(20),

        },
        scope_version_id: {
            type: DataTypes.INTEGER(20),

        },
        date: {
            type: DataTypes.DATE(20),

        },
        hours_spent: {
            type: DataTypes.INTEGER(20),

        },





    }, {
        freezeTableName: true

    })
    return ScopeEmpHours;
};