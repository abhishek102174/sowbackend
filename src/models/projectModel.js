let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ProjectDetails = sequelize.define('project_details', {
        project_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        project_name: {
            type: DataTypes.STRING(255),

        },
        project_desc: {
            type: DataTypes.STRING(255),

        }
    })
    return ProjectDetails;
};