let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeOfWork = sequelize.define('scope_of_work', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        project_id: {
            type: DataTypes.INTEGER(20),

        },
        scope_version: {
            type: DataTypes.STRING(10),

        },
        scope_name: {
            type: DataTypes.STRING(10),

        },
        start_date: {
            type: DataTypes.STRING(10),

        },
        end_date: {
            type: DataTypes.STRING(10),

        },
        total_hours: {
            type: DataTypes.STRING(10),

        },
        description: {
            type: DataTypes.STRING(255),

        },
        created_by: {
            type: DataTypes.INTEGER(20),

        },
        created_at: {
            type: DataTypes.DATE(20),

        },
        modified_by: {
            type: DataTypes.INTEGER(20),

        },
        modified_at: {
            type: DataTypes.DATE(20),

        },

    })
    return ScopeOfWork;
};