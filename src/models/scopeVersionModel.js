let { DataTypes } = require('sequelize')
module.exports = (sequelize) => {
    const ScopeVersionTable = sequelize.define('scope_version_table', {
        scope_version_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        scope_id: {
            type: DataTypes.INTEGER(20),

        },
        sub_scope_name: {
            type: DataTypes.STRING(255),

        },
        end_date: {
            type: DataTypes.DATE(20),

        },
        total_number_of_hours: {
            type: DataTypes.INTEGER(20),

        },
        description: {
            type: DataTypes.STRING(20),

        },
        created_by: {
            type: DataTypes.INTEGER(20),

        },
        created_at: {
            type: DataTypes.DATE(20),

        }
    })
    return ScopeVersionTable;
};