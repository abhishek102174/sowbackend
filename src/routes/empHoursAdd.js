const router = require('express').Router();
const empHoursController = require('../controller/empHoursController')
const { isAuthorized } = require('../../utils/auth')
router.post('/addHours/emp', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.insertEmphours(req.body);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully inserted single employee hours",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.post('/addHours/bulk', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.insertEmphoursbyTL(req.body);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully inserted employee hours by TL",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});


module.exports = router;