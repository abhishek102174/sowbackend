const router = require('express').Router();
const users = require('../controller/userController');

router.get('/userById', async function(req, res){
  try{
    const result = await users.fetchUserInfo(req);
    res.status(200);
    res.type('json');
    res.write(JSON.stringify(result));
  }  catch (err) {
    console.log(`this is error ${err}`);
    res.status(400);
  } finally {
    res.end();
  }
});

router.put('/userById', async function(req, res){
  try{
    const result = await users.updateUserInfo(req);
    res.status(200);
    res.type('json');
    res.write(JSON.stringify(result));
  }  catch (err) {
    console.log(`this is error ${err}`);
    res.status(400);
  } finally {
    res.end();
  }
});


router.post('/createUser', async function(req, res){
  // try{
    const result = await users.createUser(req, res);
  //   res.status(200);
  //   res.type('json');
  //   res.write(JSON.stringify(result));
  // }  catch (err) {
  //   console.log(`this is error ${err}`);
  //   res.status(400);
  // } finally {
  //   res.end();
  // }
});

module.exports = router;