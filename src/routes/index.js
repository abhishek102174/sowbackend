const express = require('express'),
    router = express.Router()

router.use('/', require('./user'))
router.use('/getScopes', require('./getScopes'))
router.use('/getScopebyId', require('./getscopebyScopeID'))
router.use('/getscopeversion', require('./getScopeVersion'))
router.use('/getProjects', require('./getProjects'))
router.use('/insertScope', require('./insertScope'))
router.use('/getallemp', require('./getEmp'))
router.use('/sendEmail', require('./scopeNotificationEmail'))
router.use('/updateScope', require('./updateScope'))
router.use('/insertsubscope', require('./insertSubScope'))
router.use('/getsubscope', require('./getSubScope'))
router.use('/verifylogin', require('./userLogin'))
router.use('/addinvoice', require('./addInvoice'))
router.use('/employee', require('./empHoursAdd'))
router.use('/getemployee', require('./getEmpHourDetail'))
router.use('/emailcronjob', require('../cronjob/index'))

module.exports = router