const router = require('express').Router();
const loginUserController = require('../controller/loginUserController')
const { isAuthorized } = require('../../utils/auth')
router.post('/', isAuthorized, async function(req, res) {
    try {

        const result = await loginUserController.loginUser(req.body);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "User has been successfully verified",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }


});

module.exports = router;