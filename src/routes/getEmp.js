const router = require('express').Router();
const getEmpController = require('../controller/getEmpController')
const { isAuthorized } = require('../../utils/auth')
router.get('/', isAuthorized, async function(req, res) {
    try {

        const result = await getEmpController.getAllEmployees();
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully fetched all employees",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.get('/:id', isAuthorized, async function(req, res) {
    try {

        const result = await getEmpController.getAllEmployeesbyID(req.params.id);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully fetched all employees assigned to this TL",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

module.exports = router;