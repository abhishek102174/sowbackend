const router = require('express').Router();
const getScopeController = require('../controller/getScopeController')
const { isAuthorized } = require('../../utils/auth')
router.get('/:id', isAuthorized, async function(req, res) {
    try {

        const result = await getScopeController.getScopebyProjectId(req.params.id);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Scopes has been successfully fetched",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }


});

module.exports = router;