const router = require('express').Router();
const empHoursController = require('../controller/empHoursController')
const { isAuthorized } = require('../../utils/auth')
router.get('/getHours', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.getempHoursbyEmpId(req.query);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully fetched single employee hours",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.get('/getHours/bulk', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.getempHoursbyTlId(req.query);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully fetched employee hours under this TL",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.get('/getHours/update', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.getEmpHoursbyIDforUpdate(req.query);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully fetched employee hours",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.put('/updateHours/update', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.updateEmpHoursbyempID(req.query);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            type: "Success",
            message: "Successfully updated employee hours",
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

router.get('/report', isAuthorized, async function(req, res) {
    try {

        const result = await empHoursController.getReportEmpHours(req.query);
        // res.status(200);
        // res.type('json');
        // res.write(JSON.stringify(result));
        res.status(200).send({
            data: result
        })
    } catch (err) {
        // console.log(`this is error ${err}`);
        // res.status(400);
        res.status(400).send({
            type: "error",
            status: 400,
            message: err.message,
        })
    } finally {
        res.end();
    }
});

module.exports = router;