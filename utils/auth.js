const jwt = require('jsonwebtoken');
const authConfig = require('../src/config/config')['auth']

/**
 * @param {HTTPRequest} req 
 * @returns {string} token 
 */
function getTokenFromRequest(req) {
    const authHeader = req.headers && req.headers[authConfig.jwt_auth_header_name] &&
        typeof req.headers[authConfig.jwt_auth_header_name] == 'string' ?
        req.headers[authConfig.jwt_auth_header_name] :
        null
    if (authHeader) {
        const authorization = authHeader.split(' ')
        if (authorization[0] === authConfig.jwt_auth_value_prefix) {
            return authorization[1]
        }
    }

    return null
}


/**
 * @param {string} token 
 * @returns {Promise<any>} decoded 
 */
function verifyToken(token) {

    return new Promise((resolve, reject) => {
        try {
            let error = new Error();
            if (!token) {
                error.message = 'Not Authorized to access this resource!'
                error.status = 401
                error.errorType = "Unauthorized";
                reject(error)
            }
            // var decoded = jwt.verify(token, authConfig.jwt_secret);
            // console.log(decoded)
            //verifies secret and checks exp
            jwt.verify(token, authConfig.jwt_secret, (err, decoded) => {
                if (err) {
                    error.message = 'please provide a valid token ,your token might be expired'
                    error.status = 401
                    error.errorType = "Unauthorized"
                    reject(error)
                }

                resolve(decoded)
            });
        } catch (error) {
            console.log(error)
            reject(error)
        }
    })
}

function signToken(data, algorithm = 'RS256') {

    const privateKey = authConfig.jwt_secret

    if (!data) {
        let error = new Error("please provide a valid data ,your data might be null")
        return error
    }
    var token = jwt.sign({ data: data }, privateKey);
    return token


}

async function isAuthorized(req, res, next) {
    try {
        const token = getTokenFromRequest(req);
        const decodedToken = await verifyToken(token)
            // console.log(decodedToken)
        const dataKey = authConfig.jwt_auth_data
        if (dataKey == decodedToken.data) {
            next()
        } else {
            return null
        }

    } catch (error) {
        console.log(error)
        res.status(400).send(error)
    }
}

module.exports = { isAuthorized, signToken }