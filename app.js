const express = require("express");
const port = process.env.SERVER_PORT || "8000";
const path = require('path');
const cors = require("cors");

const app = express();
const { signToken } = require('./utils/auth')
    //console.log(signToken('jwtauthheader'))
let corsOptions = {
    origin: `http://localhost:${port}`
};

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
    // res.json({ message: "Welcome to Doshaheen application." });
});

app.use(require('./src/routes.js'));
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    res.status(err.status).json({ type: 'error', message: 'the url you are trying to reach is not hosted on our server' });
    next(err);
});
app.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
});